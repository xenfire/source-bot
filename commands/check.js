exports.run = async (client, message, args) => {
	const query = require('source-server-query');
	const { MessageEmbed } = require('discord.js');

	const embed = new MessageEmbed();

	const ip = args[0];
	const port = Number(args[1]);

	if(!ip || !port) {
		embed.setTitle('Error');
		embed.setColor('RED');
		embed.setDescription(`Please provide the following arguments: **IP** and **PORT** \n \n **Example**: \`\`\`${client.config.prefix}${exports.config.name} 127.0.0.1 4356\`\`\` `);
		return message.channel.send(embed);
	}

	const info = await query.info(ip, port, 60000);
	embed.setTitle('Server Info');
	embed.setColor('BLUE');
	if(!info.name) { embed.addField('OFFLINE', `**Connection Address**: \`\`${ip}:${port}\`\``); }
	else { embed.addField(info.name, `**PLAYERS**: ${info.playersnum}/${info.maxplayers} 	**MAP**: ${info.map}    **Connection Address**: \`\`${ip}:${port}\`\``); }	return message.channel.send(embed);


};

exports.config = {
	name: 'check',
	aliases: [],
};
