exports.run = async (client, message, args) => {
	const { MessageEmbed } = require('discord.js');
	const fs = require('fs');

	const embed = new MessageEmbed();

	if (!message.member.hasPermission('ADMINISTRATOR')) {
		embed.setTitle('Error');
		embed.setColor('RED');
		embed.setDescription('You do not have permissions to execute this command. \n **Permission Needed**: ``ADMINISTRATOR``');
		return message.channel.send(embed);
	}


	if(!args[0] || message.mentions.channel) {
		embed.setTitle('Error');
		embed.setColor('RED');
		embed.setDescription(`Please provide the following argument: **CHANNEL** \n \n **EXAMPLE**: \`\`\`${client.config.prefix}${exports.config.name} #general\`\`\` `);
		return message.channel.send(embed);
	}
	else {
		fs.readFile('./servers.json', 'utf8', function readFileCallback(err, data) {
			const channel = message.mentions.channels.first();
			const parsed = JSON.parse(data);
			channel.send('DO NOT DELETE ME').then(msg => {
				parsed['channelid'] = msg.channel.id;
				parsed['messageid'] = msg.id;
				const json = JSON.stringify(parsed);
				fs.writeFile('./servers.json', json, 'utf8', () => {});
			});
		});
	}

};

exports.config = {
	name: 'setup',
	aliases: [],
};
