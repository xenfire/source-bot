const Discord = require('discord.js');
const Enmap = require('enmap');
const fs = require('fs');
const cron = require('node-cron');
const query = require('source-server-query');


const client = new Discord.Client();
const config = require('./config.json');
client.config = config;

fs.readdir('./events/', (err, files) => {
	if (err) return console.error(err);
	files.forEach(file => {
		const event = require(`./events/${file}`);
		const eventName = file.split('.')[0];
		client.on(eventName, event.bind(null, client));
	});
});

client.commands = new Enmap();

fs.readdir('./commands/', (err, files) => {
	if (err) return console.error(err);
	files.forEach(file => {
		if (!file.endsWith('.js')) return;
		const props = require(`./commands/${file}`);
		const commandName = file.split('.')[0];
		console.log(`Loading ${commandName}`);
		client.commands.set(commandName, props);
	});
});


client.on('ready', () => {
	client.user.setActivity('with status', { type: 'PLAYING' });
	console.log(` \nBot ${client.user.tag} is activated`);
	console.log(`Bot's invite link: https://discordapp.com/api/oauth2/authorize?client_id=${client.user.id}&permissions=8&scope=bot`);
});

cron.schedule('* * * * *', (async () => {
	const servers = require('./servers.json');
	if(servers.status !== 'ENABLED') return;
	const embed = new Discord.MessageEmbed();
	let currentplayers = 0;
	let maxplayers = 0;
	for (let i = 0; i < servers.ip.length; i++) {
		const ip = servers.ip[i];
		const port = servers.port[i];
		const info = await query.info(ip, port, 60000);
		if(info.playersnum) currentplayers += info.playersnum;
		if(info.maxplayers) maxplayers += info.maxplayers;
		embed.setTitle('Server Info');
		if(!info.name) { embed.addField('OFFLINE', `**Connection Address**: \`\`${ip}:${port}\`\``); }
		else { embed.addField(info.name, `**PLAYERS**: ${info.playersnum}/${info.maxplayers} 	**MAP**: ${info.map}    **Connection Address**: \`\`${ip}:${port}\`\``); }
	}
	client.channels.cache.get(servers.channelid).messages.fetch({
		around: servers.messageid,
		limit: 1,
	}).then(async m => {
		const fetchedMsg = m.first();
		embed.addField('Total Players', `${currentplayers}/${maxplayers}`);
		await fetchedMsg.edit('', { embed });
	});
}));


client.login(client.config.token);